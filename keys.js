export default function keys(obj) {
    const result = [];
    for(var key in obj) {
        result.push(key);
    }
    return result;
}
