export default function invert(obj) {
    const result = {}
    for(let key in obj) {
        let keySwitch = obj[key];
        let valueSwitch = key; 
        result[keySwitch] = valueSwitch;
    }
    return result;
}

