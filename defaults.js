export default function defaults(obj, defaultProps) {
    const result = obj;
    for(let defaults in defaultProps) {
        let flag = true;
        for(let key in obj) { 
            if(defaults === key) {
                flag = false;
            }
        }
        if(flag === true) {
            result[defaults] = defaultProps[defaults];
        }
    }
    return result;
}

