import mapObject from "../mapObject.js";

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const cb = (value) => {
    return value + " hi";
}

const result = mapObject(testObject,cb);
console.log(result);